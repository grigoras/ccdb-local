package ch.alice.o2.ccdb;

/**
 * @author costing
 * @since 2023-06-20
 */
public abstract class ConstraintOperator {
	/**
	 * Client-indicated value
	 */
	public final String value;

	/**
	 * @param value
	 */
	public ConstraintOperator(final String value) {
		this.value = value;
	}

	/**
	 * @return the SQL operator corresponding to the actual implementation
	 */
	public abstract String toSQLExpression();

	/**
	 * @return the corresponding URL operator
	 */
	public abstract String getURLExpression();

	/**
	 * @param s
	 * @return <code>true</code> if the operator
	 */
	public abstract boolean matches(String s);

	/**
	 * @return <code>true</code> if this is to be used in any object matching
	 */
	public abstract boolean isMeaningful();

	/**
	 * @return the SQL syntax that matches this constraint
	 */
	public abstract String toSQLValue();
}
