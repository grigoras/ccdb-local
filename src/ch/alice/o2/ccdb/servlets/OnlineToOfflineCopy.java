/**
 *
 */
package ch.alice.o2.ccdb.servlets;

import java.io.File;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

import ch.alice.o2.ccdb.Options;
import lazyj.Utils;

/**
 * @author costing
 * @since Sep 26, 2023
 */
public class OnlineToOfflineCopy extends Thread implements SQLNotifier {

	/**
	 * Shared logging of SQLObject* implementations
	 */
	protected static final Logger logger = Logger.getLogger(OnlineToOfflineCopy.class.getCanonicalName());

	private static int OP_COPY = 0;

	private static int OP_DELETE = 1;

	private final class Operation {
		final SQLObject object;
		final int op;

		Operation(final SQLObject object, final int op) {
			this.object = object;
			this.op = op;
		}

		public void run() {
			if (object != null && object.size > 0) {
				if (targetFolder.exists() && targetFolder.isDirectory()) {
					final File folder = new File(targetFolder, object.getFolder());

					final String targetFileName = folder.getAbsolutePath() + "/" + object.id.toString();

					if (op == OP_COPY) {
						if (!folder.exists() && !folder.mkdirs()) {
							logger.log(Level.SEVERE, "Copy failed creating target directory: " + folder.getAbsolutePath());
							return;
						}

						final File sourceFile = object.getLocalFile(false);

						// System.err.println("Source file = " + sourceFile);

						if (sourceFile == null || !sourceFile.exists() || !sourceFile.isFile() || !sourceFile.canRead()) {
							logger.log(Level.SEVERE, "Source file to copy does not exist: " + (sourceFile == null ? "not defined" : sourceFile.getAbsolutePath()));
							return;
						}

						final String sourceFileName = sourceFile.getAbsolutePath();
						if (!Utils.copyFile(sourceFileName, targetFileName))
							logger.log(Level.SEVERE, "Copy failed: " + sourceFileName + " -> " + targetFileName);
						// else
						// System.err.println("OnlineToOffline copy succeeded: " + sourceFileName + " -> " + targetFileName);
					}
					else if (op == OP_DELETE) {
						if (!folder.exists()) {
							// delete operation but the destination folder does not exist => nothing to do
							// System.err.println("Target folder does not exist: " + folder.getAbsolutePath());
							return;
						}

						final File fTarget = new File(targetFileName);

						if (fTarget.exists() && !fTarget.delete())
							logger.log(Level.SEVERE, "Could not remove: " + targetFileName);
					}
				}
			}
		}

		@Override
		public String toString() {
			return (op == OP_COPY ? "CP: " : "RM: ") + object.id.toString();
		}
	}

	private final BlockingQueue<Operation> operationsQueue = new LinkedBlockingQueue<>(1000);

	private final File targetFolder;

	/**
	 * @param targetFolder
	 *
	 */
	public OnlineToOfflineCopy(final File targetFolder) {
		setDaemon(true);

		this.targetFolder = targetFolder;
	}

	@Override
	public void run() {
		try {
			while (true) {
				final Operation op;
				try {
					op = operationsQueue.take();
				}
				catch (@SuppressWarnings("unused") final InterruptedException e) {
					return;
				}

				if (op != null) {
					try {
						op.run();
					}
					catch (final Exception e) {
						logger.log(Level.SEVERE, "Exception performing an operation", e);
						e.printStackTrace();
					}
				}
			}
		}
		finally {
			System.err.println("OnlineToOffline copy exiting");
			instance = null;
		}
	}

	@Override
	public void newObject(final SQLObject object) {
		operationsQueue.offer(new Operation(object, OP_COPY));
	}

	@Override
	public void updatedObject(final SQLObject object) {
		// not implemented
	}

	@Override
	public void deletedObject(final SQLObject object) {
		operationsQueue.offer(new Operation(object, OP_DELETE));
	}

	private static OnlineToOfflineCopy instance = null;

	static synchronized OnlineToOfflineCopy getInstace() {
		if (instance != null)
			return instance;

		final String copyFolder = Options.getOption("online.to.offline.folder", null);

		if (copyFolder == null || copyFolder.isBlank()) {
			System.err.println("OnlineToOffline copy is disabled");
			return null;
		}

		final File f = new File(copyFolder);

		if (f.exists() && f.isDirectory()) {
			instance = new OnlineToOfflineCopy(f);
			instance.start();

			System.err.println("Will copy to the Offline folder mounted on " + copyFolder);
		}
		else
			System.err.println("OnlineToOffline copy is requested but the target directory is not valid: " + copyFolder + ", will not perform this operation");

		return instance;
	}
}
