package ch.alice.o2.ccdb.tools;

import java.util.Map;
import java.util.Set;

import ch.alice.o2.ccdb.RequestParser;
import ch.alice.o2.ccdb.servlets.SQLObject;
import lazyj.DBFunctions;

/**
 * Copy objects from TPC/Config/RunInfo/ to TPC/Config/RunInfoV2, using the Redirect option instead of a tag, avoiding a second CCDB request and a file content.
 *
 * @author costing
 * @since 2025-01-23
 */
public class TPCRedirect {
	/**
	 * Entry point
	 *
	 * @param args
	 */
	public static void main(final String[] args) {
		final String sourcePath = "TPC/Config/RunInfo";
		final String targetPath = "TPC/Config/RunInfoV2";

		final Set<String> metadataCopy = Set.of("default", "Intervention", "UploadedBy", "UpdatedFrom", "runNumber", "RunType");

		try (DBFunctions db = SQLObject.getDB(); DBFunctions db2 = SQLObject.getDB()) {
			db.query("SELECT pathid FROM ccdb_paths WHERE path=?", false, sourcePath);

			final StringBuilder q = new StringBuilder(SQLObject.selectAllFromCCDB());
			q.append(" WHERE pathid=").append(db.geti("pathid")).append(" ORDER BY createTime ASC ");

			db.query(q.toString());

			while (db.moveNext()) {
				final SQLObject src = SQLObject.fromDb(db);

				RequestParser parser = new RequestParser();
				parser.path = targetPath;
				parser.startTime = src.validFrom;
				parser.notAfter = src.createTime;
				parser.notBefore = src.createTime;

				final SQLObject existing = SQLObject.getMatchingObject(parser);

				if (existing != null) {
					System.err.println("Destination already exists: " + existing.id);
					continue;
				}

				final String tag = src.getMetadataKeyValue().get("Tag");

				parser = new RequestParser();
				parser.path = "TPC/Config/FEE";
				parser.startTime = Integer.parseInt(tag);
				parser.startTimeSet = true;
				parser.notAfter = src.createTime;

				SQLObject target = SQLObject.getMatchingObject(parser);
				if (target == null) {
					System.err.println("Cannot locate a target for tag " + tag);
					return;
				}

				System.err.println("Found the following object for tag = " + tag + ":\n" + target);

				final SQLObject dest = SQLObject.fromPath(targetPath);
				dest.createTime = src.createTime;
				dest.validFrom = src.validFrom;
				dest.validUntil = src.validUntil;
				dest.lastModified = src.lastModified;
				dest.uploadedFrom = src.uploadedFrom;
				dest.initialValidity = src.initialValidity;
				dest.size = 0;

				for (final Map.Entry<String, String> entry : src.getMetadataKeyValue().entrySet())
					if (metadataCopy.contains(entry.getKey()))
						dest.setProperty(entry.getKey(), entry.getValue());

				dest.setProperty("Redirect", "/" + target.getPath() + "/" + target.validFrom + "/" + target.id);

				dest.save(null, null);
			}
		}
	}
}
