package ch.alice.o2.ccdb.tools;

import java.util.ArrayList;
import java.util.Date;

import ch.alice.o2.ccdb.servlets.SQLObject;
import lazyj.DBFunctions;
import lazyj.Utils;

/**
 * Apply the "adjustableEOV" field retroactively
 *
 * @author costing
 * @since 2025-01-22
 */
public class AdjustableEOV {
	private static boolean commit = false;
	private static boolean verboseSkip = false;
	private static boolean retrofitEOV = false;
	private static int debugPath = 0;

	/**
	 * Entry point
	 *
	 * @param args
	 */
	public static void main(final String[] args) {
		if (args.length > 0)
			commit = Utils.stringToBool(args[0], commit);

		if (args.length > 1)
			verboseSkip = Utils.stringToBool(args[1], verboseSkip);

		if (args.length > 2)
			retrofitEOV = Utils.stringToBool(args[2], retrofitEOV);

		System.err.println("Commit: " + commit);

		try (DBFunctions db = SQLObject.getDB(); DBFunctions db2 = SQLObject.getDB()) {
			if (args.length > 3) {
				try {
					debugPath = Integer.parseInt(args[3]);
				}
				catch (@SuppressWarnings("unused") final NumberFormatException nfe) {
					db.query("SELECT pathid FROM ccdb_paths WHERE path=?", false, args[3]);
					if (db.moveNext())
						debugPath = db.geti(1);
				}

				System.err.println("Debugging path " + debugPath);
			}

			final Integer id = SQLObject.getMetadataID("adjustableEOV", false);

			String qSelect = "select pathid, count(1) from ccdb where metadata->'" + id + "'='true'";

			if (debugPath > 0)
				qSelect += " and pathid=" + debugPath;

			qSelect += " group by 1 having count(1)>0 order by 2 desc;";

			System.err.println(qSelect);

			db.query(qSelect);

			int gmodified = 0;
			int ghaveAEOV = 0;
			int gremovedAEOV = 0;
			int gcannotModify = 0;
			int globalChanges = 0;

			out:
			while (db.moveNext()) {
				System.err.println("Processing " + db.geti(1) + " with " + db.geti(2) + " objects");

				final StringBuilder q = new StringBuilder(SQLObject.selectAllFromCCDB());
				q.append(" WHERE pathid=").append(db.geti("pathid")).append(" ORDER BY createTime ASC ");

				db2.query(q.toString());

				final ArrayList<SQLObject> objects = new ArrayList<>(db.geti(2));

				while (db2.moveNext()) {
					final SQLObject obj = SQLObject.fromDb(db2);
					objects.add(obj);
				}

				System.err.println("  Loaded " + objects.size() + " from DB");

				int modified = 0;
				int haveAEOV = 0;
				int removedAEOV = 0;
				int cannotModify = 0;

				for (int i = 0; i < objects.size() - 1; i++) {
					final SQLObject obj = objects.get(i);

					if ("true".equals(obj.getMetadataKeyValue().get("adjustableEOV"))) {
						haveAEOV++;
						ghaveAEOV++;
						final SQLObject next = objects.get(i + 1);

						final Object oRunNumber = obj.getMetadataKeyValue().get("runNumber");
						final Object nRunNumber = next.getMetadataKeyValue().get("runNumber");

						if (obj.validFrom < next.validFrom && obj.validUntil > next.validFrom && (obj.validUntil <= next.validUntil || (oRunNumber != null && oRunNumber.equals(nRunNumber)))) {
							if (commit) {
								if (obj.setValidityLimit(next.validFrom)) {
									obj.setProperty("adjustableEOV", null);

									if (commit) {
										if (obj.save(null, null))
											globalChanges++;

										System.err.println("After adjusting EoV:\n" + obj + "\n=======================================\n");
									}
								}
							}
							else {
								if (modified % 10 == 0)
									System.err.println("Would apply the EoV from\n" + next + "\nto\n" + obj +
											"\nthus " + new Date(obj.validUntil) + "->" + new Date(next.validFrom) + "\n-------------------\n");
							}

							modified++;
							gmodified++;
						}
						else {
							if (obj.validUntil <= next.validFrom && obj.validUntil >= next.validFrom - 2) {
								// System.err.println("Validity actually overlaps, removing the adjustableEOV from\n" + obj + "\nbecause of\n" + next + "\n%%%%%%%%%%%%%%%%%%%%%%%%%%\n");
								// System.err.println(obj.validUntil + " -> " + next.validFrom + " (" + (next.validFrom - obj.validUntil) + ")");
								// System.err.println(obj);
								obj.setValidityLimit(next.validFrom);
								obj.setProperty("adjustableEOV", null);

								if (retrofitEOV) {
									if (obj.save(null, null))
										globalChanges++;
								}

								removedAEOV++;
								gremovedAEOV++;
							}
							else {
								if (verboseSkip)
									System.err.println("Cannot modify this object:\n" + obj + "\nwith the next one:\n" + next + "\n++++++++++++++++++++++++++++++++++\n");

								cannotModify++;
								gcannotModify++;
							}
						}
					}

					if (globalChanges > 100000) {
						System.err.println("Taking a break at 100k");
						break out;
					}
				}

				System.err.println(
						"Would have modified " + modified + " objects out of " + haveAEOV + " that have the tag and " + objects.size() + " total objects in pathid=" + db.geti("pathid") + ".\n" +
								removedAEOV + " objects could drop the adjustableEOV tag and " + cannotModify + " cannot be modified based on the next object.");
			}

			System.err.println("Final stats: would have modified " + gmodified + " objects out of " + ghaveAEOV + " that have the tag.\n" +
					gremovedAEOV + " objects could drop the adjustableEOV tag and " + gcannotModify + " cannot be modified based on the next object.\nCommitted changes: " + globalChanges);
		}
	}
}
