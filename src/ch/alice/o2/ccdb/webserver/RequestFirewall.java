package ch.alice.o2.ccdb.webserver;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import ch.alice.o2.ccdb.Options;
import ch.alice.o2.ccdb.servlets.SQLBacked;
import lia.util.net.NetMatcher;

/**
 * Optional filtering of requests based on IP addresses
 *
 * @author costing
 * @since 2021-11-05
 */
public class RequestFirewall {
	private static Logger logger = Logger.getLogger(SQLBacked.class.getCanonicalName());

	private static NetMatcher deleteMatcher = null;

	static {
		final String allowedDeleteFrom = Options.getOption("allow.delete.from", "");

		if (!allowedDeleteFrom.isBlank()) {
			deleteMatcher = new NetMatcher(allowedDeleteFrom.split("[,; \r\n\t]+"));

			logger.log(Level.INFO, "DELETE requests will only be allowed from: " + deleteMatcher);
		}
	}

	/**
	 * @param request
	 * @return <code>true</code> if a removal operation is allowed
	 */
	public static boolean isDeleteAllowed(final HttpServletRequest request) {
		return deleteMatcher == null || deleteMatcher.matchInetNetwork(request.getRemoteAddr());
	}
}
