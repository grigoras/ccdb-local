package ch.alice.o2.ccdb.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import alien.monitoring.Monitor;
import alien.monitoring.MonitorFactory;
import alien.monitoring.Timing;
import ch.alice.o2.ccdb.RequestParser;
import ch.alice.o2.ccdb.servlets.formatters.FormatterFactory;
import ch.alice.o2.ccdb.servlets.formatters.SQLFormatter;
import lazyj.DBFunctions;
import lazyj.Format;
import lazyj.Utils;

/**
 * SQL-backed implementation of CCDB. This servlet implements browsing the object tree, without any actual object listing. For that see SQLBrowse.
 *
 * @author costing
 * @since 2024-03-25
 */
@WebServlet("/tree/*")
public class SQLTree extends ExtraVerbsServlet {
	private static final long serialVersionUID = 1L;

	private static final Monitor monitor = MonitorFactory.getMonitor(SQLTree.class.getCanonicalName());

	@Override
	public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
		try (Timing t = new Timing(monitor, "GET_ms")) {
			final RequestParser parser = new RequestParser(request, true);

			final List<Integer> pathIDs = SQLObjectImpl.getPathIDsWithPatternFallback(parser);

			final SQLFormatter formatter = FormatterFactory.getFormatter(request, parser.latestFlag);

			formatter.setParser(parser);

			response.setContentType(formatter.getContentType());

			CCDBUtils.disableCaching(response);
			CCDBUtils.disableRobots(response);

			final boolean sizeReport = Utils.stringToBool(request.getParameter("report"), false);

			formatter.setExtendedReport(sizeReport);

			try (PrintWriter pw = response.getWriter()) {
				formatter.start(pw);

				formatter.header(pw);
				formatter.footer(pw);

				formatter.subfoldersListingHeader(pw);

				long thisFolderCount = 0;
				long thisFolderSize = 0;

				String query;

				if (parser.wildcardMatching) {
					if (sizeReport)
						query = "SELECT path, object_count, object_size FROM ccdb_paths LEFT OUTER JOIN ccdb_stats USING (pathid) WHERE pathid in (";
					else
						query = "SELECT path FROM ccdb_paths WHERE pathid in (";

					query += Format.toCommaList(pathIDs) + " )";
				}
				else {
					if (sizeReport)
						query = "SELECT path, object_count, object_size FROM ccdb_paths LEFT OUTER JOIN ccdb_stats USING (pathid) WHERE path ~ '^" + Format.escSQL(parser.path) + "/[^/]+$'";
					else
						query = "SELECT path FROM ccdb_paths WHERE path ~ '^" + Format.escSQL(parser.path) + "/[^/]+$'";
				}

				query += " ORDER BY 1;";

				try (DBFunctions db = SQLObject.getDB()) {
					db.query(query);

					boolean first = true;

					while (db.moveNext()) {
						if (first)
							first = false;
						else
							formatter.middle(pw);

						String folder = db.gets(1);

						if (sizeReport) {
							formatter.subfoldersListing(pw, folder, folder, db.getl(2), db.getl(3), 0, 0);
							thisFolderCount += db.getl(2);
							thisFolderSize += db.getl(3);
						}
						else
							formatter.subfoldersListing(pw, folder, folder);
					}
				}

				formatter.subfoldersListingFooter(pw, thisFolderCount, thisFolderSize);

				formatter.end(pw);
			}

			t.endTiming();

			RequestLogging.logRequest("/tree", "GET", request, response, parser, t);
		}
	}
}
