package ch.alice.o2.ccdb.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ch.alice.o2.ccdb.Options;

/**
 * Extension to support the PATCH HTTP verb
 *
 * @author costing
 * @since 2022-09-19
 */
public class ExtraVerbsServlet extends HttpServlet {
	private static final long serialVersionUID = 6623154952767126836L;

	private static final boolean UA_SANITY_CHECKS = lazyj.Utils.stringToBool(Options.getOption("ua.sanity.checks", null), false);

	@Override
	protected void service(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		if (UA_SANITY_CHECKS) {
			final String userAgent = req.getHeader("User-Agent");

			if (userAgent == null || userAgent.isBlank()) {
				resp.sendError(418, "I'm a teapot");
				RequestLogging.logRequest(req.getServletPath(), req.getMethod(), req, resp, null, null);
				return;
			}
		}

		if ("PATCH".equals(req.getMethod()))
			this.doPatch(req, resp);
		else if ("MOVE".equals(req.getMethod()))
			this.doMove(req, resp);
		else
			super.service(req, resp);
	}

	/**
	 * @param req
	 * @param resp
	 * @throws ServletException
	 * @throws IOException
	 */
	@SuppressWarnings("static-method")
	protected void doPatch(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		resp.sendError(HttpServletResponse.SC_NOT_IMPLEMENTED, "PATCH method not implemented by this endpoint");
		RequestLogging.logRequest(req.getPathInfo(), req.getMethod(), req, resp, null, null);
	}

	/**
	 * @param req
	 * @param resp
	 * @throws ServletException
	 * @throws IOException
	 */
	@SuppressWarnings("static-method")
	protected void doMove(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		resp.sendError(HttpServletResponse.SC_NOT_IMPLEMENTED, "MOVE method not implemented by this endpoint");
		RequestLogging.logRequest(req.getPathInfo(), req.getMethod(), req, resp, null, null);
	}
}
