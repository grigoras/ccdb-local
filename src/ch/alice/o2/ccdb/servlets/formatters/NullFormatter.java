package ch.alice.o2.ccdb.servlets.formatters;

import java.io.PrintWriter;

import ch.alice.o2.ccdb.RequestParser;
import ch.alice.o2.ccdb.multicast.Blob;
import ch.alice.o2.ccdb.servlets.LocalObjectWithVersion;
import ch.alice.o2.ccdb.servlets.SQLObject;

/**
 * @author costing
 * @since 2022-05-23
 */
class NullFormatter implements SQLFormatter {
	@Override
	public void header(final PrintWriter writer) {
		// nothing
	}

	@Override
	public void format(final PrintWriter writer, final SQLObject obj) {
		// nothing
	}

	@Override
	public void format(final PrintWriter writer, final LocalObjectWithVersion obj) {
		// nothing
	}

	@Override
	public void footer(final PrintWriter writer) {
		// nothing
	}

	@Override
	public void middle(final PrintWriter writer) {
		// nothing
	}

	@Override
	public void start(final PrintWriter writer) {
		// nothing
	}

	@Override
	public void subfoldersListingHeader(final PrintWriter writer) {
		// nothing
	}

	@Override
	public void subfoldersListing(final PrintWriter writer, final String path, final String url) {
		// nothing
	}

	@Override
	public void subfoldersListing(final PrintWriter writer, final String path, final String url, final long ownCount, final long ownSize, final long subfolderCount, final long subfolderSize) {
		// nothing
	}

	@Override
	public void subfoldersListingFooter(final PrintWriter writer, final long ownCount, final long ownSize) {
		// nothing
	}

	@Override
	public void end(final PrintWriter writer) {
		// nothing
	}

	@Override
	public void setExtendedReport(final boolean extendedReport) {
		// nothing
	}

	@Override
	public void format(final PrintWriter writer, final Blob obj) {
		// nothing
	}

	@Override
	public String getContentType() {
		return "text/empty";
	}

	@Override
	public void setParser(final RequestParser parser) {
		// ignore
	}
}
