/**
 *
 */
package ch.alice.o2.ccdb.servlets;

import lazyj.DBFunctions;

/**
 * @author costing
 * @since Nov 2, 2022
 */
public class IDCCleanup {
	private static final Integer localReplica = Integer.valueOf(0);

	/**
	 * @param args
	 */
	public static void main(final String[] args) {
		long deletedFiles = 0;
		long deletedSize = 0;

		try (DBFunctions db = SQLObject.getDB()) {
			db.query("select *,round(extract(epoch from lower(validity))*1000) as validfrom,round(extract(epoch from upper(validity))*1000) as validuntil from ccdb inner join ccdb_paths using(pathid) where path like 'TPC/Calib/IDC_DELTA_%' and -1 = any(replicas) and 0 = any(replicas);");

			final SQLLocalRemoval removal = SQLLocalRemoval.getInstance();

			while (db.moveNext()) {
				final SQLObject object = SQLObject.fromDb(db);

				//System.err.println("Cleaning up local replica of: "+object);

				removal.deletedObject(object);

				if (!object.replicas.contains(localReplica)) {
					deletedFiles++;
					deletedSize += object.size;
					object.updateReplicas();
				}
			}
		}

		final AsyncReplication instance = AsyncReplication.getInstance(null);

		if (instance!=null)
		    instance.shutDown();

		System.err.println("Deleted files: "+deletedFiles+", amount of disk space reclaimed: "+lazyj.Format.size(deletedSize));
	}
}
