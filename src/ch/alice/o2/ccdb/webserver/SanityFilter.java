package ch.alice.o2.ccdb.webserver;

import java.io.IOException;
import java.util.Set;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Check if the User-Agent is sane
 *
 * @author costing
 * @since 2023-09-08
 */
public class SanityFilter extends HttpFilter {

	private static final long serialVersionUID = 1L;

	private static final Set<String> rejectedStrings = Set.of("<script", "scaninfo@paloaltonetworks.com", "robertdavidgraham", "research@pdrlabs.net", "internet-measurement.com",
			"about.censys.io", "leakix.net");

	@Override
	protected void doFilter(final HttpServletRequest request, final HttpServletResponse response, final FilterChain chain) throws IOException, ServletException {
		String ua = request.getHeader("User-Agent");

		boolean acceptableRequest = true;

		if (ua != null) {
			ua = ua.toLowerCase();

			for (final String s : rejectedStrings)
				if (ua.contains(s)) {
					acceptableRequest = false;
					break;
				}
		}

		if (acceptableRequest)
			chain.doFilter(request, response);
		else
			response.sendError(418, "I'm a teapot");
	}
}
