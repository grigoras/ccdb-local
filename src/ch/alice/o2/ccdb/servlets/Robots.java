package ch.alice.o2.ccdb.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Discourage robots from indexing this data
 *
 * @author costing
 * @since 2022-03-31
 */
public class Robots extends HttpServlet {
	private static final long serialVersionUID = 1602850779943763143L;

	@Override
	protected void doGet(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		resp.setContentType("text/plain");
		try (ServletOutputStream os = resp.getOutputStream()) {
			os.println("User-agent: *");
			os.println("Disallow: /");
		}
	}
}
