# Recovery procedures

## Restore database from backup

Backups of the entire database are taken daily with `pg_dumpall` from one of the Offline nodes (currently `alicdb2`). As such they are available on the common shared storage in the `alicdb2/` subfolder, and in the similar location (`/opt/backup/toTape/alicdb2/`) on the TSM CERN service.

Restore process description: [PostgreSQL documentation](https://www.postgresql.org/docs/13/backup-dump.html#BACKUP-DUMP-RESTORE).
## Change master node
todo:
- promote replica to master
- restart (?) pgpool
- change settings in Bucardo 
tbh we can have scripts for case of failing every node, without generic instructions 

## Change node, where Bucardo is running
todo:
- restore configuration of Bucardo on node alicdb4
- start service Bucardo on alicdb4
- change in monitoring

[back](readme.md)