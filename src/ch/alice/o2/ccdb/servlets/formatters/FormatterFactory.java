/**
 *
 */
package ch.alice.o2.ccdb.servlets.formatters;

import java.util.HashSet;
import java.util.Set;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;

/**
 * @author costing
 * @since Apr 27, 2020
 */
public class FormatterFactory {
	static final Set<String> BLOB_IGNORE_MDKEYS = Set.of("Created", "InitialValidityLimit", "Valid-Until", "Last-Modified", "Valid-From", "OriginalFileName", "File-Size", "Content-MD5",
			"Content-Type");

	/**
	 * @param request
	 * @param latest if <code>true</code> then point the client to the "/latest/" endpoint instead of the default "/browse/" one for subfolder navigation
	 * @return the best formatter according to the requested content type by the client
	 */
	public static SQLFormatter getFormatter(final HttpServletRequest request, final boolean latest) {
		if (request == null)
			return new NullFormatter();

		SQLFormatter formatter = null;

		String sAccept = request.getParameter("Accept");

		if ((sAccept == null) || (sAccept.length() == 0))
			sAccept = request.getHeader("Accept");

		if ((sAccept == null || sAccept.length() == 0))
			sAccept = "text/plain";

		sAccept = sAccept.toLowerCase();

		Set<String> fieldFilter = null;

		final String filterHeader = request.getHeader("X-Filter-Fields");

		if (filterHeader != null && !filterHeader.isBlank()) {
			final StringTokenizer st = new StringTokenizer(filterHeader, " \r\t\n,;");

			while (st.hasMoreTokens()) {
				final String tok = st.nextToken().trim();

				if (tok.length() > 0) {
					if (fieldFilter == null)
						fieldFilter = new HashSet<>();

					fieldFilter.add(tok);
				}
			}
		}

		if ((sAccept.indexOf("application/json") >= 0) || (sAccept.indexOf("text/json") >= 0))
			formatter = new JSONFormatter(fieldFilter);
		else if (sAccept.indexOf("text/html") >= 0) {
			final HTMLFormatter html = new HTMLFormatter();

			if (latest)
				html.setBrowseEndpoint("latest");

			formatter = html;

		}
		else if (sAccept.indexOf("text/xml") >= 0)
			formatter = new XMLFormatter();
		else
			formatter = new TextFormatter();

		return formatter;
	}
}
